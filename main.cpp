#include <pylon/BaslerUniversalInstantCamera.h>
#include <pylon/GrabResultPtr.h>
#include <pylon/PylonIncludes.h>
#include <iostream>
#include <vector>
#include <algorithm>

typedef Pylon::CBaslerUniversalInstantCamera BaslerUniversalCamera;

void connectTwoCameras() {
    std::cout << "Initializing..." << std::endl;

    Pylon::PylonInitialize();
    Pylon::CTlFactory &tl_factory = Pylon::CTlFactory::GetInstance();
    Pylon::DeviceInfoList_t all_devices;
    Pylon::CDeviceInfo info;
    Pylon::DeviceInfoList_t filter_;

    info.SetTLType(Pylon::TLType::TLTypeUSB);
    filter_.push_back(info);
    std::cout << "Populating cameras list" << std::endl;
    tl_factory.EnumerateDevices(all_devices, filter_, true);

    if (all_devices.empty()) {
        std::cout << "Couldn't find any camera connected." << std::endl;
    } else {
        std::cout << "All camera devices after filter (num of cameras- " << std::to_string(all_devices.size()) << "):" << std::endl;
        for (Pylon::CDeviceInfo cam_device : all_devices) {
            std::cout << cam_device.GetModelName() << " , " << cam_device.GetDeviceGUID() << " , "
                      << cam_device.GetSerialNumber() << ", " << cam_device.GetDeviceIdx() << std::endl;
        }
    }

    std::vector<Pylon::IPylonDevice *> devices;
    std::vector<int> camera_serials;
    std::cout << "Creating devices (num of devices- " << std::to_string(all_devices.size()) << "):" << std::endl;
    for (int i = 0; i < all_devices.size(); i++) {
        try {
            std::stringstream ss;
            ss << all_devices[i].GetSerialNumber();
            int serial = stoi(ss.str());
            if (std::find(camera_serials.begin(), camera_serials.end(), serial) == camera_serials.end()) { // don't create existing device
                std::cout << "Creating device " << ss.str() << std::endl;
                camera_serials.push_back(serial);
                devices.push_back(tl_factory.CreateDevice(all_devices[i]));
            } else {
                std::cout << "Existing device " << ss.str() << ", not creating duplication" << std::endl;
            }
        } catch (std::exception &e) {
            std::cout << "ERROR parsing camera serial" << std::endl;
        }
    }

    std::vector<BaslerUniversalCamera *> cameras;
    std::cout << "Creating cameras (num of cameras- " << std::to_string(devices.size()) << "):" << std::endl;
    for (Pylon::IPylonDevice *device : devices) {
        std::cout << "Creating new camera " << device->GetDeviceInfo().GetSerialNumber() << std::endl;
        cameras.push_back(new BaslerUniversalCamera(device));
    }

    std::cout << "Opening cameras (num of cameras- " << std::to_string(cameras.size()) << "):" << std::endl;
    for (BaslerUniversalCamera* camera : cameras) {
        camera->Open();
        std::cout << "Resetting camera " << camera->DeviceSerialNumber.GetValue() << std::endl;
        camera->DeviceReset.Execute();
        camera->Close();
    }
}

int main() {
    connectTwoCameras();
    return 0;
}