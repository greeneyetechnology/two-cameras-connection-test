# Two Cameras start One stops
## Brif
In the situation of two Basler cameras starting almost at the same time when connected via USB, one of them stop working.

## Build and run
There are several ways to run:
## Pull image from Docker Hub and Run
* ```docker pull lioriz/basler-two-cameras-test-arm```
* ```docker run --rm -it --privileged lioriz/basler-two-cameras-test-arm```
## Build from Dockerfile and Run
* ```docker build -t two-cameras-test-arm --progress=plain -f DockerfileArm .``` (--progress=plain is just to see the build logs)
* ```docker run --rm -it --privileged two-cameras-test-arm```
## Build from Cmake and run
* ```mkdir build && cd build```
* ```cmake -DARM=on ..``` or ```cmake -DX86=on ..```
* ```make -j4```
* ```./Two_Cameras_Test```
### Build from tar (deprecated)
run ```docker load -i two-cameras-test-arm.tar``` from the base dir
* FYI: to save the build image as tar you run ```docker save -o <tar name> <image name>```, example ```docker save -o two-cameras-test-arm.tar b67001e7e1aa```

run ```docker run --rm -it --privileged <image name>```, example ```docker run --rm -it --privileged b67001e7e1aa```