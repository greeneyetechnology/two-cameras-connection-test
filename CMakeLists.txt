set(CMAKE_VERBOSE_MAKEFILE on)

cmake_minimum_required(VERSION 3.13)

set(PROJECT_NAME "Two_Cameras_Test")

project(${PROJECT_NAME})

option(X86 "Linux X86 compilation" OFF)
option(ARM "Linux ARM compilation" OFF)

set(CMAKE_CXX_FLAGS_DEBUG_INIT "-Wall -g")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
set(INCLUDE_ROOT "/usr/include")

#### Pylon
set(PYLON_ROOT "/usr/local/pylon")
include_directories(${PYLON_ROOT}/include)
include_directories(${PYLON_ROOT}/include/pylonc)
include_directories(${PYLON_ROOT}/include/GenApi)
include_directories(${PYLON_ROOT}/include/base)
include_directories(${PYLON_ROOT}/include/pylon/gige)
include_directories(${PYLON_ROOT}/include/pylon/private)
link_directories(${PYLON_ROOT}/lib)
link_directories(${PYLON_ROOT}/include)
link_directories(${PYLON_ROOT})

if(ARM)
    set(pylon_LIBS
            "GCBase_gcc_v3_1_Basler_pylon"
            "GenApi_gcc_v3_1_Basler_pylon"
            "pylonbase-6.1.0"
            )
elseif(X86)
    set(pylon_LIBS
            "GCBase_gcc_v3_1_Basler_pylon"
            "GenApi_gcc_v3_1_Basler_pylon"
            "pylonbase-6.1.1"
            )
else()
    message(FATAL_ERROR "Please define ARM or X86, see README for build instruction.")
endif()


######## main exe
add_executable(${PROJECT_NAME} main.cpp)

target_link_libraries(${PROJECT_NAME} ${pylon_LIBS} )